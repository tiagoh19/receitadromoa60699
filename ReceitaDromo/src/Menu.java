import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.border.LineBorder;
import javax.swing.JEditorPane;

public class Menu extends JFrame {

	private JPanel contentPane;
	private JTextField tb_nome;
	private JTextField tb_n_doses;
	private JTextField tb_tempo_prep;
	private JTextField tb_tempo_confe;
	private JTextField tb_classi;
	private JTextField tb_observ;
	private JTextField tb_tipo_receita;
	private JTextField textField;
	private JTextField tb_ver_nome;
	private JTextField tb_ver_n_doses;
	private JTextField tb_ver_tempo_prepa;
	private JTextField tb_ver_tempo_confe;
	private JTextField tb_ver_classi;
	private JTextField tb_ver_obser;
	private JTextField tb_ver_receita;
	private JTextField procurar_receita_editar;
	private JTextField nome_editar;
	private JTextField n_doses_editar;
	private JTextField tempo_prep_editar;
	private JTextField tempo_confe_editar;
	private JTextField tempo_class_editar;
	private JTextField obser_editar;
	private JTextField tipoReceita_editar;
	private JTextField nomeIngrediente;
	private JTextField caloriasIngredientes;
	private JTextField precoIngredientes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			// Ingrediente Gravar
			JPanel GuardarIngrediente = new JPanel();
			GuardarIngrediente.setVisible(false);
			// Ver receita
			JPanel VerReceita = new JPanel();
			VerReceita.setBackground(Color.WHITE);
			VerReceita.setVisible(false);
			VerReceita.setBounds(112, 0, 705, 459);
			contentPane.add(VerReceita);
			VerReceita.setLayout(null);
			
			JList list = new JList();
			list.setBorder(new LineBorder(new Color(0, 0, 0)));
			list.setBounds(151, 60, -129, 388);
			VerReceita.add(list);
			
					
					JLabel lblNewLabel_2 = new JLabel("Introduza o numero da receita :");
					lblNewLabel_2.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					lblNewLabel_2.setBounds(71, 11, 226, 14);
					VerReceita.add(lblNewLabel_2);
					
					textField = new JTextField();
					textField.setBounds(255, 9, 86, 20);
					VerReceita.add(textField);
					textField.setColumns(10);
					
					JLabel label = new JLabel("Nome :");
					label.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label.setBounds(339, 41, 62, 17);
					VerReceita.add(label);
					
					JLabel label_1 = new JLabel("N\u00BA de Doses :");
					label_1.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_1.setBounds(339, 76, 91, 17);
					VerReceita.add(label_1);
					
					JLabel label_2 = new JLabel("Tempo Prepara\u00E7\u00E3o :");
					label_2.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_2.setBounds(339, 104, 115, 17);
					VerReceita.add(label_2);
					
					JLabel label_3 = new JLabel("Tempo confeca\u00E7\u00E3o :");
					label_3.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_3.setBounds(339, 132, 128, 17);
					VerReceita.add(label_3);
					
					JLabel label_4 = new JLabel("Classifica\u00E7\u00E3o :");
					label_4.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_4.setBounds(339, 160, 115, 17);
					VerReceita.add(label_4);
					
					JLabel label_5 = new JLabel("Observa\u00E7\u00F5es :");
					label_5.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_5.setBounds(339, 188, 115, 17);
					VerReceita.add(label_5);
					
					JLabel label_6 = new JLabel("Tipo Receita :");
					label_6.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
					label_6.setBounds(339, 216, 115, 17);
					VerReceita.add(label_6);
					
					JButton btnNewButton = new JButton("Procurar");
					btnNewButton.setBounds(351, 8, 89, 23);
					VerReceita.add(btnNewButton);
					
					tb_ver_nome = new JTextField();
					tb_ver_nome.setEditable(false);
					tb_ver_nome.setColumns(10);
					tb_ver_nome.setBounds(465, 40, 145, 20);
					VerReceita.add(tb_ver_nome);
					
					tb_ver_n_doses = new JTextField();
					tb_ver_n_doses.setEditable(false);
					tb_ver_n_doses.setColumns(10);
					tb_ver_n_doses.setBounds(465, 75, 145, 20);
					VerReceita.add(tb_ver_n_doses);
					
					tb_ver_tempo_prepa = new JTextField();
					tb_ver_tempo_prepa.setEditable(false);
					tb_ver_tempo_prepa.setColumns(10);
					tb_ver_tempo_prepa.setBounds(465, 103, 145, 20);
					VerReceita.add(tb_ver_tempo_prepa);
					
					tb_ver_tempo_confe = new JTextField();
					tb_ver_tempo_confe.setEditable(false);
					tb_ver_tempo_confe.setColumns(10);
					tb_ver_tempo_confe.setBounds(465, 131, 145, 20);
					VerReceita.add(tb_ver_tempo_confe);
					
					tb_ver_classi = new JTextField();
					tb_ver_classi.setEditable(false);
					tb_ver_classi.setColumns(10);
					tb_ver_classi.setBounds(465, 160, 145, 20);
					VerReceita.add(tb_ver_classi);
					
					tb_ver_obser = new JTextField();
					tb_ver_obser.setEditable(false);
					tb_ver_obser.setColumns(10);
					tb_ver_obser.setBounds(465, 187, 145, 20);
					VerReceita.add(tb_ver_obser);
					
					tb_ver_receita = new JTextField();
					tb_ver_receita.setEditable(false);
					tb_ver_receita.setColumns(10);
					tb_ver_receita.setBounds(465, 215, 145, 20);
					VerReceita.add(tb_ver_receita);
					
					JButton btt_loadValues = new JButton("Carregar Elementos");
					btt_loadValues.setBounds(268, 285, 133, 38);
					VerReceita.add(btt_loadValues);
					btt_loadValues.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection c = null;
							Statement stmt = null;
							try {
							  Class.forName("org.sqlite.JDBC");
							  c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.db");
							  c.setAutoCommit(false);
							  
							  ArrayList<receita> Listareceita2 = new ArrayList<receita>();
							  
							  stmt = c.createStatement();
							  ResultSet rs = stmt.executeQuery( "SELECT * FROM receita;" );

							  
							  while ( rs.next() ) {
								  receita obj = new receita(rs.getInt("id_receita"),rs.getString("nome"),rs.getInt("n_doses"),rs.getInt("tempo_preparacao"),rs.getInt("tempo_confecacao"),rs.getInt("classificacao_utilizador"),rs.getString("observacoes_utilizador"),rs.getString("tipo_receita"));
							      Listareceita2.add(obj);	
							      String receitalist = Integer.toString(rs.getInt("id_receita")) + " - " + rs.getString("nome");
							      DefaultListModel dm = new DefaultListModel();
							       dm.addElement(receitalist);
							       list.setModel(dm);
							  }
							  rs.close();
							  stmt.close();
							  c.close();
							} catch ( Exception e1 ) {
							  System.err.println( e1.getClass().getName() + ": " + e1.getMessage() );
							  System.exit(0);
							  
							}
						}
						
						
					});
					
					
					
			GuardarIngrediente.setBackground(Color.WHITE);
			GuardarIngrediente.setBounds(112, 0, 705, 459);
			contentPane.add(GuardarIngrediente);
			GuardarIngrediente.setLayout(null);
			
			JLabel lblNewLabel_3 = new JLabel("Nome :");
			lblNewLabel_3.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblNewLabel_3.setBounds(25, 38, 52, 17);
			GuardarIngrediente.add(lblNewLabel_3);
			
			nomeIngrediente = new JTextField();
			nomeIngrediente.setBounds(94, 37, 125, 19);
			GuardarIngrediente.add(nomeIngrediente);
			nomeIngrediente.setColumns(10);
			
			JLabel lblCalorias = new JLabel("Calorias :");
			lblCalorias.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblCalorias.setBounds(25, 71, 52, 17);
			GuardarIngrediente.add(lblCalorias);
			
			caloriasIngredientes = new JTextField();
			caloriasIngredientes.setColumns(10);
			caloriasIngredientes.setBounds(94, 68, 125, 19);
			GuardarIngrediente.add(caloriasIngredientes);
			
			JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o");
			lblDescrio.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblDescrio.setBounds(25, 108, 74, 17);
			GuardarIngrediente.add(lblDescrio);
			
			JLabel lblPreo = new JLabel("Pre\u00E7o :");
			lblPreo.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			lblPreo.setBounds(352, 38, 52, 17);
			GuardarIngrediente.add(lblPreo);
			
			precoIngredientes = new JTextField();
			precoIngredientes.setColumns(10);
			precoIngredientes.setBounds(414, 37, 125, 19);
			GuardarIngrediente.add(precoIngredientes);
			
			JEditorPane descricaoIngredientes = new JEditorPane();
			descricaoIngredientes.setText("Escreve Aqui...");
			descricaoIngredientes.setForeground(Color.BLACK);
			descricaoIngredientes.setBounds(25, 136, 201, 141);
			GuardarIngrediente.add(descricaoIngredientes);
			
			JButton btt_GravarIngrediente = new JButton("Gravar Ingrediente");
			btt_GravarIngrediente.setBounds(414, 284, 156, 40);
			GuardarIngrediente.add(btt_GravarIngrediente);
			btt_GravarIngrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						String nome = nomeIngrediente.toString();
						int calorias = 0;
						String descricao = descricaoIngredientes.toString();
						float preco = 0;
						
						try {
							calorias = Integer.parseInt(caloriasIngredientes.toString());
						} catch (NumberFormatException e1) {
						
						}
						try {
							preco = Float.parseFloat(precoIngredientes.toString());
						} catch (NumberFormatException e1) {
							
						}						
						
						Ingrediente.inserir_Ingredientes(nome, calorias, descricao, preco);
					}
					catch(Exception e1) {
						
						
					}
					
			
				}
				
				
			});
			// Ingrediente Menu
			JPanel IngredienteMenu = new JPanel();
			IngredienteMenu.setBackground(Color.WHITE);
			IngredienteMenu.setBounds(112, 0, 705, 459);
			contentPane.add(IngredienteMenu);
			IngredienteMenu.setLayout(null);
			
	
			// Receita Menu
			JPanel receitap = new JPanel();
			receitap.setBackground(Color.WHITE);
			receitap.setBounds(112, 0, 705, 459);
			contentPane.add(receitap);
			receitap.setLayout(null);
			
			// Guardar Receita
			JPanel GuardarReceita = new JPanel();
			GuardarReceita.setVisible(false);
			GuardarReceita.setBackground(Color.WHITE);
			GuardarReceita.setBounds(112, 0, 705, 459);
			contentPane.add(GuardarReceita);
			GuardarReceita.setLayout(null);
			
			// Editar receita
			JPanel EditarReceita = new JPanel();
			EditarReceita.setBackground(Color.WHITE);
			EditarReceita.setBounds(112, 0, 705, 459);
			contentPane.add(EditarReceita);
			EditarReceita.setLayout(null);
			
			
			JButton bttGuardarIngrediente = new JButton("Guardar Ingredinte");
			bttGuardarIngrediente.setBounds(68, 143, 125, 70);
			IngredienteMenu.add(bttGuardarIngrediente);
			bttGuardarIngrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					EditarReceita.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(true);
				}
				
				
			});
			JButton bttVerReceita = new JButton("Ver Receita");
			bttVerReceita.setBounds(203, 143, 125, 70);
			IngredienteMenu.add(bttVerReceita);
			
			JButton btnEditarReceita = new JButton("Editar Receita");
			btnEditarReceita.setBounds(338, 143, 125, 70);
			IngredienteMenu.add(btnEditarReceita);
			
			JButton btnApagarIngrediente = new JButton("Apagar Ingrediente");
			btnApagarIngrediente.setBounds(473, 143, 137, 70);
			IngredienteMenu.add(btnApagarIngrediente);
			
			
			JButton Ingrediente = new JButton("Ingrediente");
			Ingrediente.setBounds(10, 60, 96, 38);
			contentPane.add(Ingrediente);
			Ingrediente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					EditarReceita.setVisible(false);
					IngredienteMenu.setVisible(true);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			JButton Compras = new JButton("Compras");
			Compras.setBounds(10, 109, 96, 38);
			contentPane.add(Compras);
			
			JButton Receita = new JButton("Receita");
			Receita.setBounds(10, 11, 96, 38);
			contentPane.add(Receita);
			Receita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(true);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					EditarReceita.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			
			JButton verReceita = new JButton("Ver Receita");
			verReceita.setBounds(73, 149, 121, 81);
			receitap.add(verReceita);
			verReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(true);
					EditarReceita.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			JButton addReceita = new JButton("Adicionar Receita");
			addReceita.setBounds(204, 149, 121, 81);
			receitap.add(addReceita);
			addReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(true);
					VerReceita.setVisible(false);
					EditarReceita.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			JButton editarReceita = new JButton("Editar Receita");
			editarReceita.setBounds(335, 149, 121, 81);
			receitap.add(editarReceita);
			editarReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(false);
					VerReceita.setVisible(false);
					EditarReceita.setVisible(true);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			JButton apagarReceita = new JButton("Apagar Receita");
			apagarReceita.setBounds(466, 149, 121, 81);
			receitap.add(apagarReceita);
			editarReceita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					receitap.setVisible(false);
					GuardarReceita.setVisible(true);
					VerReceita.setVisible(false);
					IngredienteMenu.setVisible(false);
					GuardarIngrediente.setVisible(false);
				}
				
				
			});
			
			
			
			JList list_1 = new JList();
			list_1.setBorder(new LineBorder(new Color(0, 0, 0)));
			list_1.setBounds(216, 63, -129, 388);
			EditarReceita.add(list_1);
			
			JLabel label_7 = new JLabel("Introduza o numero da receita :");
			label_7.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_7.setBounds(136, 14, 226, 14);
			EditarReceita.add(label_7);
			
			procurar_receita_editar = new JTextField();
			procurar_receita_editar.setColumns(10);
			procurar_receita_editar.setBounds(320, 12, 86, 20);
			EditarReceita.add(procurar_receita_editar);
			
			JLabel label_8 = new JLabel("Nome :");
			label_8.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_8.setBounds(404, 44, 62, 17);
			EditarReceita.add(label_8);
			
			JLabel label_9 = new JLabel("N\u00BA de Doses :");
			label_9.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_9.setBounds(404, 79, 91, 17);
			EditarReceita.add(label_9);
			
			JLabel label_10 = new JLabel("Tempo Prepara\u00E7\u00E3o :");
			label_10.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_10.setBounds(404, 107, 115, 17);
			EditarReceita.add(label_10);
			
			JLabel label_11 = new JLabel("Tempo confeca\u00E7\u00E3o :");
			label_11.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_11.setBounds(404, 135, 128, 17);
			EditarReceita.add(label_11);
			
			JLabel label_12 = new JLabel("Classifica\u00E7\u00E3o :");
			label_12.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_12.setBounds(404, 163, 115, 17);
			EditarReceita.add(label_12);
			
			JLabel label_13 = new JLabel("Observa\u00E7\u00F5es :");
			label_13.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_13.setBounds(404, 191, 115, 17);
			EditarReceita.add(label_13);
			
			JLabel label_14 = new JLabel("Tipo Receita :");
			label_14.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
			label_14.setBounds(404, 219, 115, 17);
			EditarReceita.add(label_14);
			
			JButton btt_procurarEditar = new JButton("Procurar");
			btt_procurarEditar.setBounds(416, 11, 89, 23);
			EditarReceita.add(btt_procurarEditar);
			
			nome_editar = new JTextField();
			nome_editar.setColumns(10);
			nome_editar.setBounds(530, 43, 145, 20);
			EditarReceita.add(nome_editar);
			
			n_doses_editar = new JTextField();
			n_doses_editar.setColumns(10);
			n_doses_editar.setBounds(530, 78, 145, 20);
			EditarReceita.add(n_doses_editar);
			
			tempo_prep_editar = new JTextField();
			tempo_prep_editar.setColumns(10);
			tempo_prep_editar.setBounds(530, 106, 145, 20);
			EditarReceita.add(tempo_prep_editar);
			
			tempo_confe_editar = new JTextField();
			tempo_confe_editar.setColumns(10);
			tempo_confe_editar.setBounds(530, 134, 145, 20);
			EditarReceita.add(tempo_confe_editar);
			
			tempo_class_editar = new JTextField();
			tempo_class_editar.setColumns(10);
			tempo_class_editar.setBounds(530, 163, 145, 20);
			EditarReceita.add(tempo_class_editar);
			
			obser_editar = new JTextField();
			obser_editar.setColumns(10);
			obser_editar.setBounds(530, 190, 145, 20);
			EditarReceita.add(obser_editar);
			
			tipoReceita_editar = new JTextField();
			tipoReceita_editar.setColumns(10);
			tipoReceita_editar.setBounds(530, 218, 145, 20);
			EditarReceita.add(tipoReceita_editar);
			
			JButton btt_editar = new JButton("Editar Receita");
			btt_editar.setBounds(554, 272, 121, 23);
			EditarReceita.add(btt_editar);
			EditarReceita.setVisible(false);
			
			
				
				
				
			
		
		
		
		
	
		
		
		
		
		
		
		JLabel lblNewLabel = new JLabel("Nome :");
		lblNewLabel.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 28, 62, 17);
		GuardarReceita.add(lblNewLabel);
		
		JLabel lblNDeDoses = new JLabel("N\u00BA de Doses :");
		lblNDeDoses.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblNDeDoses.setBounds(10, 63, 91, 17);
		GuardarReceita.add(lblNDeDoses);
		
		JLabel lblTempoPreparao = new JLabel("Tempo Prepara\u00E7\u00E3o :");
		lblTempoPreparao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTempoPreparao.setBounds(10, 91, 115, 17);
		GuardarReceita.add(lblTempoPreparao);
		
		JLabel lblTempoConfecao = new JLabel("Tempo confeca\u00E7\u00E3o :");
		lblTempoConfecao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTempoConfecao.setBounds(10, 119, 128, 17);
		GuardarReceita.add(lblTempoConfecao);
		
		JLabel lblClassificao = new JLabel("Classifica\u00E7\u00E3o :");
		lblClassificao.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblClassificao.setBounds(10, 147, 115, 17);
		GuardarReceita.add(lblClassificao);
		
		JLabel lblObservaes = new JLabel("Observa\u00E7\u00F5es :");
		lblObservaes.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblObservaes.setBounds(10, 175, 115, 17);
		GuardarReceita.add(lblObservaes);
		
		JLabel lblTipoReceita = new JLabel("Tipo Receita :");
		lblTipoReceita.setFont(new Font("Microsoft JhengHei", Font.PLAIN, 12));
		lblTipoReceita.setBounds(10, 203, 115, 17);
		GuardarReceita.add(lblTipoReceita);
		
		tb_nome = new JTextField();
		tb_nome.setBounds(136, 27, 145, 20);
		GuardarReceita.add(tb_nome);
		tb_nome.setColumns(10);
		
		tb_n_doses = new JTextField();
		tb_n_doses.setColumns(10);
		tb_n_doses.setBounds(136, 62, 145, 20);
		GuardarReceita.add(tb_n_doses);
		
		tb_tempo_prep = new JTextField();
		tb_tempo_prep.setColumns(10);
		tb_tempo_prep.setBounds(136, 90, 145, 20);
		GuardarReceita.add(tb_tempo_prep);
		
		tb_tempo_confe = new JTextField();
		tb_tempo_confe.setColumns(10);
		tb_tempo_confe.setBounds(136, 118, 145, 20);
		GuardarReceita.add(tb_tempo_confe);
		
		tb_classi = new JTextField();
		tb_classi.setColumns(10);
		tb_classi.setBounds(136, 147, 145, 20);
		GuardarReceita.add(tb_classi);
		
		tb_observ = new JTextField();
		tb_observ.setColumns(10);
		tb_observ.setBounds(136, 174, 145, 20);
		GuardarReceita.add(tb_observ);
		
		tb_tipo_receita = new JTextField();
		tb_tipo_receita.setColumns(10);
		tb_tipo_receita.setBounds(136, 202, 145, 20);
		GuardarReceita.add(tb_tipo_receita);
		
		JLabel lblNewLabel_1 = new JLabel("* Escolha de 1-5");
		lblNewLabel_1.setBounds(297, 149, 91, 14);
		GuardarReceita.add(lblNewLabel_1);
		
		JButton btt_gravar_receita = new JButton("Gravar Receita");
		btt_gravar_receita.setBounds(333, 261, 115, 32);
		GuardarReceita.add(btt_gravar_receita);
		btt_gravar_receita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String nome = tb_nome.toString();
					int n_doses = 0;
					int temp_prepa = 0;
					int tempo_confe = 0;
					int classi = 0;
					try {
						n_doses = Integer.parseInt(tb_n_doses.toString());
					} catch (NumberFormatException e1) {
					
					}
					try {
						temp_prepa = Integer.parseInt(tb_tempo_prep.toString());
					} catch (NumberFormatException e1) {
						
					}
					try {
						 tempo_confe = Integer.parseInt(tb_tempo_confe.toString());
					} catch (NumberFormatException e1) {
					
					}
					try {
						classi = Integer.parseInt(tb_classi.toString());
					} catch (NumberFormatException e1) {
					
					}
					String obser =  tb_observ.toString();
					String tipo_receita = tb_tipo_receita.toString();
					receita.inserir_receita(nome,n_doses,temp_prepa,tempo_confe , classi , obser , tipo_receita);
				}
				catch(Exception e1) {
					
					
				}
				
			}
			
			
		});
		
	}
}
