import java.awt.Cursor;
import java.awt.List;
import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.sqlite.SQLiteDataSource;
public class receita {
		
		int id_receita;
		String nome;
		int n_doses;
		int tempo_prep;
		int tempo_conf;
		int classificacao;
		String observ;
		String tipo_receita;
		
	  
	   
	  public receita(int id_receita, String nome, int n_doses, int tempo_prep, int tempo_conf, int classificacao,
				String observ, String tipo_receita) {
			super();
			this.id_receita = id_receita;
			this.nome = nome;
			this.n_doses = n_doses;
			this.tempo_prep = tempo_prep;
			this.tempo_conf = tempo_conf;
			this.classificacao = classificacao;
			this.observ = observ;
			this.tipo_receita = tipo_receita;
		}

	public static void connectDB()
	  {
	      Connection c = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	  }
	   
	  public static void inserir_receita(String nome, int n_doses, int tempo_prep, int tempo_conf, int classificacao,
				String observ, String tipo_receita)
	  {
	      	Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	          c.setAutoCommit(false);
	          
	 
	          stmt = c.createStatement();
	          stmt.executeUpdate("INSERT INTO `Receita`(`nome`, `n_doses`, `tempo_preparacao`, `tempo_confecacao`, `classificacao_utilizador`, `observacoes_utilizador`, `link_foto`, `tipo_receita`) "
	        		  				+ "VALUES ('"+ nome + "',"+ n_doses + ", " + tempo_prep + " , " + tempo_conf + " , " + classificacao + " , '" + observ + "' , '"+ "Nada"+ "' , '" + tipo_receita + "')");
	          
	          stmt.close();
	          c.commit();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	        JOptionPane.showMessageDialog(null, "Gravado sucesso");
	  }
	  private ArrayList<receita> Listareceita  = LerRegistos();
	   
	  public  ArrayList<receita> LerRegistos()
	  {
	        Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.sqlite");
	          c.setAutoCommit(false);
	          
	          ArrayList<receita> Listareceita2 = new ArrayList<receita>();
	          
	          stmt = c.createStatement();
	          ResultSet rs = stmt.executeQuery( "SELECT * FROM Receita;" );
	       
	          
	          while ( rs.next() ) {
	        	  receita obj = new receita(rs.getInt("id_receita"),rs.getString("nome"),rs.getInt("n_doses"),rs.getInt("tempo_preparacao"),rs.getInt("tempo_confecacao"),rs.getInt("classificacao_utilizador"),rs.getString("observacoes_utilizador"),rs.getString("tipo_receita"));
	              Listareceita2.add(obj);	            
	              return Listareceita2;
	          }
	          rs.close();
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	          
	        }
	        return null;
	        
	  }
	  
	  
	  public static void updateDB()
	  {
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:myBlog.sqlite");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");
	 
	      stmt = c.createStatement();
	      String sql = "UPDATE web_blog set message = 'This is updated by updateDB()' where ID=1;";
	      stmt.executeUpdate(sql);
	      c.commit();
	 
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM web_blog;" );
	      while ( rs.next() ) {
	         int id = rs.getInt("id");
	         String  name = rs.getString("name");
	         String  message = rs.getString("message");
	         String date_added = rs.getString("date_added");
	         System.out.println( "ID : " + id );
	         System.out.println( "Name : " + name );
	         System.out.println( "Message : " + message );
	         System.out.println( "Date Added : " + date_added );
	         System.out.println();
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	  }
	   
	  public static void deleteDB()
	  {
	      Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:myBlog.sqlite");
	          c.setAutoCommit(false);
	          System.out.println("Opened database successfully");
	 
	          stmt = c.createStatement();
	          String sql = "DELETE from web_blog where ID=1;";
	          stmt.executeUpdate(sql);
	          c.commit();
	         
	          ResultSet rs = stmt.executeQuery( "SELECT * FROM web_blog;" );
	          while ( rs.next() ) {
	        	  
	             int id = rs.getInt("id");
	             String  name = rs.getString("name");
	             String  message = rs.getString("message");
	             String date_added = rs.getString("date_added");
	             System.out.println( "ID : " + id );
	             System.out.println( "Name : " + name );
	             System.out.println( "Message : " + message );
	             System.out.println( "Date Added : " + date_added );
	             System.out.println();
	          }
	          rs.close();
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        System.out.println("Operation done successfully");
	  }
}
