import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class Ingrediente {
	
	int id_ingredinte;
	String Nome;
	int calorias;
	String descricao;
	float preco;
	

	public Ingrediente(int id_ingredinte, String nome, int calorias, String descricao, float preco) {
		super();
		this.id_ingredinte = id_ingredinte;
		Nome = nome;
		this.calorias = calorias;
		this.descricao = descricao;
		this.preco = preco;
	}

	public static void connectDB()
	  {
	      Connection c = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	  }
	   
	  public static void inserir_Ingredientes(String nome, int calorias, String descricao, float preco)
				
	  {
	      	Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\tiago\\Desktop\\workspace\\ReceitaDromo\\ReceitaDromo.db");
	          c.setAutoCommit(false);
	          
	 
	          stmt = c.createStatement();
	          stmt.executeUpdate("INSERT INTO `Ingredientes`(`Nome`, `calorias`, `descricao`, `preco`) "
	        		  				+ "VALUES ('"+ nome + "',"+ calorias + ", '" + descricao + "' , " + preco + "')");
	          
	          stmt.close();
	          c.commit();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        
	        JOptionPane.showMessageDialog(null, "Gravado sucesso");
	  }
	  private ArrayList<Ingrediente> ListaIngredientes  = LerRegistos();
	   
	  public  ArrayList<Ingrediente> LerRegistos()
	  {
	        Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:ReceitaDromo.sqlite");
	          c.setAutoCommit(false);
	          
             
	          ArrayList<Ingrediente> ListaIngrediente = new ArrayList<Ingrediente>();
	          stmt = c.createStatement();
	          ResultSet rs = stmt.executeQuery( "SELECT * FROM Receita;" );
	         
	          
	          while ( rs.next() ) {
	        	  Ingrediente obj = new Ingrediente(rs.getInt("Id_ingrediente"),rs.getString("Nome"),rs.getInt("calorias"),rs.getString("descricao"),rs.getInt("preco"));
	        	  ListaIngrediente.add(obj);	            
	             return ListaIngrediente;
	          }
	          rs.close();
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	          
	        }
	        return null;
	        
	  }
	   
	  public static void updateDB()
	  {
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:myBlog.sqlite");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");
	 
	      stmt = c.createStatement();
	      String sql = "UPDATE web_blog set message = 'This is updated by updateDB()' where ID=1;";
	      stmt.executeUpdate(sql);
	      c.commit();
	 
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM web_blog;" );
	      while ( rs.next() ) {
	         int id = rs.getInt("id");
	         String  name = rs.getString("name");
	         String  message = rs.getString("message");
	         String date_added = rs.getString("date_added");
	         System.out.println( "ID : " + id );
	         System.out.println( "Name : " + name );
	         System.out.println( "Message : " + message );
	         System.out.println( "Date Added : " + date_added );
	         System.out.println();
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	  }
	   
	  public static void deleteDB()
	  {
	      Connection c = null;
	        Statement stmt = null;
	        try {
	          Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:myBlog.sqlite");
	          c.setAutoCommit(false);
	          System.out.println("Opened database successfully");
	 
	          stmt = c.createStatement();
	          String sql = "DELETE from web_blog where ID=1;";
	          stmt.executeUpdate(sql);
	          c.commit();
	 
	          ResultSet rs = stmt.executeQuery( "SELECT * FROM web_blog;" );
	          while ( rs.next() ) {
	             int id = rs.getInt("id");
	             String  name = rs.getString("name");
	             String  message = rs.getString("message");
	             String date_added = rs.getString("date_added");
	             System.out.println( "ID : " + id );
	             System.out.println( "Name : " + name );
	             System.out.println( "Message : " + message );
	             System.out.println( "Date Added : " + date_added );
	             System.out.println();
	          }
	          rs.close();
	          stmt.close();
	          c.close();
	        } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	        }
	        System.out.println("Operation done successfully");
	  }
}


